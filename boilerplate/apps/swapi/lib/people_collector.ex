defmodule Swapi.PeopleCollector do
  @moduledoc """
  Author(s): Nathan Casados (marth141 @ github)

  This is a GenSever that collects people from SWAPI and stores them in Postgres
  """
  use GenServer
  alias Db.Repo
  alias Swapi.People

  # GenServer (Functions)

  def start_link(_) do
    GenServer.start_link(
      # module genserver will call
      __MODULE__,
      # init params
      [],
      name: __MODULE__
    )
  end

  # GenServer (Callbacks)

  def init(_opts) do
    {:ok, %{last_refresh: nil}, {:continue, :init}}
  end

  def handle_info(:refresh_cache, state) do
    refresh_cache()
    {:noreply, %{state | last_refresh: DateTime.utc_now()}}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  def handle_continue(:init, state) do
    case Application.get_env(:swapi, :env) do
      :dev ->
        {:noreply, %{state | last_refresh: DateTime.utc_now()}}

      :prod ->
        schedule_init_poll()
        {:noreply, %{state | last_refresh: DateTime.utc_now()}}
    end
  end

  # Private Helpers

  def refresh_cache() do
    Repo.delete_all(Swapi.People)

    stream_people_from_swapi()
    |> Task.async_stream(&insert_or_update_postgres/1, timeout: :infinity)
    |> Stream.run()

    IO.puts("\n \n ======= Swapi People Refreshed ======= \n \n")
    schedule_poll()
    :ok
  end

  # Todo it might be that if a project stage is completed, that the record is complete
  defp insert_or_update_postgres(swapi_people) do
    try do
      existing = Repo.get_by!(Swapi.People, url: swapi_people["url"])

      existing
      |> People.update(swapi_people)
    rescue
      _ -> People.create(swapi_people)
    end
  end

  defp schedule_poll do
    Process.send_after(self(), :refresh_cache, :timer.minutes(30))
  end

  defp schedule_init_poll do
    Process.send_after(self(), :refresh_cache, :timer.minutes(1))
  end

  defp stream_people_from_swapi() do
    Swapi.stream_people()
  end
end
