defmodule Web.SwapiPlanetsApiController do
  @moduledoc """
  This is the Controller for the "/api/planets" endpoint
  """
  use Web, :controller

  @doc """
  Lists planets from SWAPI
  """
  def list(conn, _params) do
    results =
      Db.Repo.all(Swapi.Planets)
      |> Enum.map(fn struct -> struct |> Map.from_struct() |> Map.delete(:__meta__) end)
      |> Enum.map(fn planet ->
        Map.update!(planet, :residents, fn residents ->
          Enum.map(residents, fn resident ->
            resident = Db.Repo.get_by!(Swapi.People, url: resident)
            resident.name
          end)
        end)
      end)

    json(conn, %{results: results})
  end

  def list_post(conn, params) do
    IO.inspect(conn)

    json(conn, %{status: :ok})
  end
end
