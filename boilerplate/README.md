# GoSOLO Node Challenge

This Elixir Umbrella App accomplishes getting some records from Star Wars API (SWAPI) as a consumer, but also provides a mutation of Planets on the "/api/planets" endpoint.

To run, one should have Elixir installed.

Setup the application with `mix deps.get`, `mix ecto.create`, and `mix ecto.migrate` to retrieve dependencies and create the Postgres tables.

In the "boilerplate/" directory, run `iex -S mix phx.server` to start the server and application.

`Swapi.refresh_caches` will retrieve people and planet data from SWAPI.

Once data is retrieved, one could make a simple GET request to `localhost:4000/api/planets` and one would retrieve planets where the residents are mutated to list the names of residents, rather than their URL.

One would find the API endpoint defined in the `apps/web/lib/web/router.ex` file.

Under `apps/swapi` one would find the collectors and schema defintions for the retrieved SWAPI records.

Under `apps/db/priv/repo/migrations` one would find where the Postgres tables are defined.

May the force be with you!
